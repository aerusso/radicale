Read Me
=======

Radicale is a free and open-source CalDAV and CardDAV server.

For the complete documentation, please visit
[Radicale "2.1" documentation](https://radicale.org/2.1.html).
